import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { ServerComponent } from './server/server.component';
import { SeccuccAlertComponent } from './seccucc-alert/seccucc-alert.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import { Assigmrnt3Component } from './assigmrnt3/assigmrnt3.component';
@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    SeccuccAlertComponent,
    WarningAlertComponent,
    Assigmrnt3Component
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
