import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  disabledNewServer = true;
  isServerCreated = 'No server Created !';
  updateServerName = '';
  itemValue = '';

  name = 'Rayan';
  date  = new Date();
  constructor() {
    setTimeout(() => {
       this.disabledNewServer =  false;
    }, 2000);
  }

  onCreateServer() {
    this.isServerCreated = 'There is Server Created ';
  }
  onUpdateServerName(event: Event) {
             this.updateServerName = (event.target as HTMLInputElement).value;

  }
  itemSelected(event: Event) {
    this.itemValue = (event.target as HTMLSelectElement).value;

  }
}
