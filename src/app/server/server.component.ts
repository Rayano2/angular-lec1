import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
    isServerCreated = false;
    inputvalue = '';

  constructor() { }

  ngOnInit() {
  }

  createServer() {
    this.isServerCreated = true;
    setTimeout(() => {
      this.isServerCreated = false;
    }, 10000);
  }


}
